//
//  File.swift
//  
//
//  Created by Behrad Kazemi on 6/17/22.
//

import Foundation
struct API {
  public static let baseUrl = "https://api.bitso.com/v3"
}

protocol Endpoint {
  var path: String { get }
  var url: String { get }
}

enum Endpoints {
  
  public enum PublicRestAPI: Endpoint {
    case ticker(String)
    case availableBooks
    public var path: String {
      switch self {
      case .availableBooks:
        return "/available_books/"
      case .ticker(let book):
        return "/ticker/?book=\(book)"
      }
    }
    public var url: String {
      return "\(API.baseUrl)\(path)"
    }
  }
}
